﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Lasika.Controllers {
    [Serializable]
    public class Player : MonoBehaviour {
        protected const float MaxFoodLevel = 100;
        protected const float MaxThirstLevel = 100;
        protected const float MaxHealth = 100;

        public float ThirstModifier = 1;
        public float FoodModifier = 1;
        public float HealthModifier = 1;

        public float CurrentFoodLevel;
        public float CurrentThirstLevel;
        public float CurrentHealth;

        public Slider FoodSlider;
        public Slider ThirstSlider;
        public Slider HealthSlider;

        public bool IsDead;

        // Use this for initialization
        protected virtual void Start() {
            CurrentFoodLevel = MaxFoodLevel;
            CurrentHealth = MaxHealth;
            CurrentThirstLevel = MaxThirstLevel;
            InvokeRepeating(nameof(Starve), 2f, 1f);
        }

        public virtual void Eat(int amount) {
            if (amount + CurrentFoodLevel >= MaxFoodLevel) CurrentFoodLevel = MaxFoodLevel;
            else CurrentFoodLevel += amount;
        }

        public virtual void Drink(int amount) {
            if (amount + CurrentThirstLevel >= MaxThirstLevel) CurrentThirstLevel = MaxThirstLevel;
            else CurrentThirstLevel += amount;
        }

        public virtual void Heal(int amount) {
            if (amount + CurrentHealth >= MaxHealth) CurrentHealth = MaxHealth;
            else CurrentHealth += amount;
        }

        private void Starve() {
            if ((CurrentThirstLevel <= 0 || CurrentFoodLevel <= 0) && CurrentHealth >= 0) {
                Damage(HealthModifier);
            }

            if (CurrentThirstLevel >= 0) {
                CurrentThirstLevel -= ThirstModifier;
                if (ThirstSlider != null)
                    ThirstSlider.value = CurrentThirstLevel / 100;
            }

            if (CurrentFoodLevel >= 0) {
                CurrentFoodLevel -= FoodModifier;
                if (FoodSlider != null)
                    FoodSlider.value = CurrentFoodLevel / 100;
            }

            if (CurrentHealth <= 0) {
                Die();
            }
        }

        protected virtual void Die() {
            IsDead = true;
        }


        public virtual void Damage(float amount) {
            CurrentHealth -= amount;
            if (HealthSlider != null)
                HealthSlider.value = CurrentHealth / 100;
            if (CurrentHealth <= 0) {
                Die();
            }
        }
    }
}