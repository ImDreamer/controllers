﻿using System;
using UnityEngine;

namespace Players {
    public class PointAndClickController : MonoBehaviour {
        public Transform Target;
        public Vector3 Offset;
        protected Camera Camera;
        protected const float ZoomSpeed = 2f;

        // Use this for initialization
        private void Start() {
            Camera = GetComponent<Camera>();
        }

        // Update is called once per frame
        protected virtual void Update() {
            if (Math.Abs(Input.GetAxisRaw("Mouse ScrollWheel")) > 0) {
                Camera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * ZoomSpeed;
                Camera.orthographicSize = Mathf.Clamp(Camera.orthographicSize, 0.5f, 10);
            }

            if (Input.GetMouseButton(2)) {
                Offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * 3, Vector3.up) * Offset;
            }

            transform.position = Target.position + Offset;
            transform.LookAt(Target.position);
        }
    }
}