﻿using UnityEngine;
using UnityEngine.AI;
using Lasika.Utility;

namespace Lasika.Controllers.PAC.InteractionSystem {
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(Controls))]
    [RequireComponent(typeof(Player))]
    public class WorldInteraction : MonoBehaviour {
        protected NavMeshAgent PlayerAgent;
        protected Player Player;

        protected virtual void Start() {
            PlayerAgent = GetComponent<NavMeshAgent>();
            Player = GetComponent<Player>();
        }

        protected virtual void Update() {
            if (Player.IsDead) return;
            if ((UnityEngine.EventSystems.EventSystem.current == null ||
                 UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) &&
                UnityEngine.EventSystems.EventSystem.current != null) return;
            if (Input.GetMouseButtonDown(0)) {
                GetInteraction();
            }
        }

        protected virtual void GetInteraction() {
            System.Diagnostics.Debug.Assert(Camera.main != null, "Camera.main != null");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out RaycastHit rayCastHit, Mathf.Infinity)) return;
            GameObject hitGameObject = rayCastHit.collider.gameObject;
            Interactable intractable = hitGameObject.GetComponent<Interactable>();
            if (intractable != null) {
                intractable.MoveToInteraction(PlayerAgent);
                Debug.Log("WorldInteraction::GetInteraction -> Interacted Successfully with " + hitGameObject.name);
            } else {
                PlayerAgent.SetDestination(rayCastHit.point);
                PlayerAgent.stoppingDistance = 0;
                //Debug.Log("WorldInteraction::GetInteraction -> Successfully moved to " + rayCastHit.point);
            }
        }
    }
}