﻿using UnityEngine;
using UnityEngine.AI;

namespace Lasika.Controllers.PAC.InteractionSystem {
    public class Interactable : MonoBehaviour {
        public float StoppingDistance = 3f;

        [HideInInspector] public NavMeshAgent PlayerAgent;
        protected bool HasInteracted;

        public virtual void MoveToInteraction(NavMeshAgent playerAgent) {
            HasInteracted = false;
            PlayerAgent = playerAgent;
            PlayerAgent.SetDestination(transform.position);
            playerAgent.stoppingDistance = StoppingDistance;
        }

        private void Update() {
            if (HasInteracted || PlayerAgent == null || !PlayerAgent.hasPath) return;
            if (!(PlayerAgent.remainingDistance <= PlayerAgent.stoppingDistance)) return;
            Interact();
            LookAtTarget();
            HasInteracted = true;
        }

        private void LookAtTarget() {
            PlayerAgent.updateRotation = false;
            PlayerAgent.transform.LookAt(new Vector3(transform.position.x, PlayerAgent.transform.position.y,
                transform.position.z));
            PlayerAgent.updateRotation = true;
        }

        public virtual void Interact() {
            Debug.Log("Interactable::Interact -> Interacting with base class on " + gameObject.name);
        }
    }
}