﻿using UnityEngine;

namespace Lasika.Controllers.PAC.InteractionSystem.ActionItem {
    public class ActionItem : Interactable {

        public override void Interact() {
            Debug.Log("ActionItem::Interact -> Interacted with ActionItem class");
        }
    }
}