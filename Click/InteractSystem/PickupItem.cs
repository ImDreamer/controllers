﻿using UnityEngine;

namespace Lasika.Controllers.PAC.InteractionSystem {
    public class PickupItem : Interactable {
        public override void Interact() {
            Debug.Log("Item::Interact -> Interacted with Item class");
        }
    }
}